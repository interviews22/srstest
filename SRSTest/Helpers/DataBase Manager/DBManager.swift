//
//  DBManager.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//

import CoreData


class DBManager {
    static let shared = DBManager()
    private init() { }
    
    /// Container object to hold db
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "SRSTest")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // FUTURE STATE: Handle error
            }
        })
        return container
    }()

    /// Context
    lazy var context: NSManagedObjectContext = {
        let context = persistentContainer.newBackgroundContext()
        context.automaticallyMergesChangesFromParent = true
        return context
    }()
    
    /// Method to save context
    func saveContext () {
        let context = persistentContainer.viewContext
        guard context.hasChanges else {
            return
        }
        do {
            try context.save()
        } catch {
            // FUTURE STATE: Handle error
        }
    }
}
