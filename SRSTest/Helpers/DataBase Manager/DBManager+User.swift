//
//  DBManager+User.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import CoreData

extension DBManager {
    /// Method to save user to core data
    func saveUser(firstName: String,
                  lastName: String,
                  phoneNumber: String,
                  dateOfBirth: String,
                  signature: Data,
                  completion: @escaping (User?) -> ()) {
        context.perform {
            do {
                
                let user = User(context: self.context)
                user.firstName = firstName
                user.lastName = lastName
                user.phoneNumber = phoneNumber
                user.dob = dateOfBirth
                user.signature = signature
                try self.context.save()
                DispatchQueue.main.async {
                    completion(user)
                }
            }
            catch {
                // FUTURE-STATE: Handle error
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
    
    
    /// Method to fetch users from core data
    func fetchUsers(completion: @escaping ([User]) -> ()) {
        context.perform {
            do {
                let fetchRequest: NSFetchRequest<User>
                fetchRequest = User.fetchRequest()
                let users = try self.context.fetch(fetchRequest)
                DispatchQueue.main.async {
                    completion(users)
                }
            }
            catch {
                // FUTURE-STATE: Handle error
                DispatchQueue.main.async {
                    completion([])
                }
            }
        }
    }
}

