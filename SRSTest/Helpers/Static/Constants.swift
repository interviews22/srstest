//
//  Constants.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//

enum Constants: String {
    case warning = "Warning"
    case success = "Success"
    case done = "Done"
    case dateFormat = "dd/MM/yyyy"
    case invalidFirstName = "Type a valid First Name"
    case invalidLastName = "Type a valid Last Name"
    case invalidPhoneNumber = "Type a vlid phone number"
    case invalidDateOfBirth = "Choose your date of birth"
    case invalidSignature = "Draw a valid signature"
    case userSaveError = "Sorry, Unfortunately we can't save your data at the moment! Try again later."
    case users = "Users"
    case author = "Sarath Raveendran"
    case creator = "SR Inc."
    case termsAndConditions = "Terms and Conditions"
    case preview = "Preview"
    case signHere = "Sign Here"
    case usersListIsEmpty = "Users list is empty"
}

extension Constants {
    var text: String {
        return self.rawValue
    }
}
