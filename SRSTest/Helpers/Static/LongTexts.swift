//
//  LongTexts.swift
//  SRSTest
//
//  Created by SRN on 4/4/22.
//

struct LongTexts {
    static let consent = """
User consent represents a user's explicit permission to allow an application to access resources protected by scopes. Consent grants are different from tokens because a consent can outlast a token, and there can be multiple tokens with varying sets of scopes derived from a single consent.

When an application needs to get a new access token from an authorisation server, the user isn't prompted for consent if they have already consented to the specified scopes. Consent grants remain valid until the user or admin manually revokes them, or until the user, application, authorisation server, or scope is deactivated or deleted.
"""
}
