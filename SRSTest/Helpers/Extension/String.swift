//
//  String.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import UIKit

/// Extension for string utility
extension String {
    var isValidFirstName: Bool {
        let regEx = "([a-zA-Z', .-]+( [a-zA-Z', .-]+)*){3,30}"
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        return test.evaluate(with: self)
    }
    
    var isValidLastName: Bool {
        let regEx = "([a-zA-Z', .-]+( [a-zA-Z', .-]+)*){1,30}"
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        return test.evaluate(with: self)
    }
    
    var isValidPhoneNumber: Bool {
        let character  =  CharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: character)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
}
