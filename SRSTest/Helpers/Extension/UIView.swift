//
//  UIView.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import UIKit

extension UIView {
    /// Method to add shadow on view
    /// - Parameters:
    ///     - color: UIColor
    ///     - opacity: Float
    ///     - offset: CGSize
    ///     - radius: CGFloat
    func addShadow(color: UIColor = .black,
                   opacity: Float = 0.3,
                   offset: CGSize = .zero,
                   radius: CGFloat = 2) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
    
    /// Method to set Visual format constraints
    /// - Parameters:
    ///     - format: String
    ///     - views: Array of UIView
    ///     - metrics: [String : Any]
    func addConstraintsWithFormat(_ format: String,
                                  views: UIView...) {
        
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format,
                                                      options: NSLayoutConstraint.FormatOptions(),
                                                      metrics: [:],
                                                      views: viewsDictionary))
    }
}
