//
//  UserTableViewCell.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import UIKit

extension UITableViewCell {
    static var identifier: String {
        String(describing: self).components(separatedBy: ".").last!
    }
}
