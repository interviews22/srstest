//
//  EmptyView.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import UIKit

class EmptyView: UIView {
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }()
    
    var title: String?
    var image: UIImage?
    var imageTintColor: UIColor?
    
    // MARK: Life Cycle
    init(image: UIImage?,
         title: String?,
         imageTintColor: UIColor? = .black) {
        super.init(frame: .zero)
        self.image = image
        self.title = title
        self.imageTintColor = imageTintColor
        self.configureView()
    }
    
    required  init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        configureView()
    }
    
    /// Method to arrange view
    func configureView() {
        
        backgroundColor = .white
        addSubview(imageView)
        addSubview(titleLabel)
        
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -60).isActive = true
        let size = round(UIScreen.main.bounds.width * 0.5)
        imageView.widthAnchor.constraint(equalToConstant: size).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: round(size * 3 / 4)).isActive = true
        
        titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 40).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24).isActive = true
        setData()
    }
    
    /// Method to set data
    func setData() {
        titleLabel.text = title
        imageView.image = image
        imageView.tintColor = imageTintColor
    }
}
