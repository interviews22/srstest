//
//  EmptyView+Delegate.swift
//  SRSTest
//
//  Created by SRN on 4/4/22.
//
import UIKit

protocol EmptyViewDelegate: AnyObject {
    var emptyView: EmptyView { get set }
    func addEmptyViewAsSubView(_ tintColor: UIColor?)
    func showEmptyView()
    func hideEmptyView()
}

extension EmptyViewDelegate where Self: UIViewController {
    /// Method to arrange view
    func addEmptyViewAsSubView(_ tintColor: UIColor? = nil) {
        if let _tintColor = tintColor {
            emptyView.imageView.tintColor = _tintColor
        }
        emptyView.isHidden = true
        view.addSubview(emptyView)
        view.addConstraintsWithFormat("H:|-(0)-[v0]-(0)-|", views: emptyView)
        view.addConstraintsWithFormat("V:|-(0)-[v0]-(0)-|", views: emptyView)
    }
    
    /// Method to empty view
    func showEmptyView() {
        emptyView.isHidden = false
        self.view.bringSubviewToFront(self.emptyView)
    }
    
    /// Method to hide empty view
    func hideEmptyView() {
        self.emptyView.isHidden = true
    }
}
