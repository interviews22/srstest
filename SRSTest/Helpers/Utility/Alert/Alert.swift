//
//  Alert.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import UIKit
import SwiftMessages

class Alert {
    static let shared = Alert()
    private init() { }
   
    /// Method to present alert
    /// - Parameters:
    ///     - message: String
    ///     - isSuccess: true or false
    func present(message: String,
                 isSuccess: Bool = true) {
        show(isSuccess ? .success : .warning,
             title: isSuccess ? Constants.success.text : Constants.warning.text,
             message: message,
             position: .top,
             duration: .automatic)
    }

    /// Method to configure alert
    /// - Parameters:
    ///     - theme: success or failure
    ///     - title: String,
    ///     - message: String,
    ///     - position: Top, Bottom or Middle
    ///     - duration: Duration in seconds
    private func show(_ theme: Theme,
                      title: String,
                      message: String,
                      position: SwiftMessages.PresentationStyle,
                      duration: SwiftMessages.Duration) {
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureTheme(theme)
        view.button?.isHidden = true
        view.configureContent(title: title, body: message)
     
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        config.presentationStyle = position
        config.duration = duration
        SwiftMessages.show(config: config, view: view)
    }
}
