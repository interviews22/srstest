//
//  Alert+Delegate.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//

/// Delegate to access alert
protocol AlertDelegate {
    func success(message: String)
    func failure(message: String)
}


extension AlertDelegate {
    /// Method to present success alert
    /// - Parameters:
    ///     - message: String
    func success(message: String) {
        Alert.shared.present(message: message, isSuccess: true)
    }

    /// Method to present failure alert
    /// - Parameters:
    ///     - message: String
    func failure(message: String) {
        Alert.shared.present(message: message, isSuccess: false)
    }
}
