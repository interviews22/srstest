//
//  StoryboardInstantiable.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import UIKit

protocol StoryboardInitializable {
    static var viewControllerIdentifier: String { get }
    static var storyboardName: String { get }
    static var bundle: Bundle { get }
}

extension StoryboardInitializable where Self: UIViewController {
    static var storyboardName: String {
        "Main"
    }
    
    static var bundle: Bundle {
        Bundle.main
    }
    
    static var storyboardInstance: Self {
        let storyboard = UIStoryboard(name: storyboardName, bundle: bundle)
        let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerIdentifier)
        return viewController as! Self
    }
    
    static var viewControllerIdentifier: String {
        guard let id = String(describing: self).components(separatedBy: ".").last else {
            fatalError("Story board identifier not found!")
        }
        return id
    }
}
