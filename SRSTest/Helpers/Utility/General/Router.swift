//
//  Router.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//

import UIKit

protocol Router {
    var presenter: UIViewController { get set }
    func present(controller: UIViewController)
    func push(controller: UIViewController)
}


extension Router {
    /// Method to present controller over current presentation
    func present(controller: UIViewController) {
        presenter.present(controller,
                          animated: true,
                          completion: nil)
    }
    
    /// Method to push view controller on current navigation stack
    func push(controller: UIViewController) {
            self.presenter.navigationController?.pushViewController(controller,
                                                                    animated: true)
    }
}
