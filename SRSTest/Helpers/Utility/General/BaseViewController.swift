//
//  BaseController.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import UIKit


class BaseViewController: UIViewController {
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        loadData()
    }
    
    func configureView() {
        
    }
    
    func loadData() {
        
    }
}
