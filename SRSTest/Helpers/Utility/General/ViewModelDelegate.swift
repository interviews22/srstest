//
//  ViewModelDelegate.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//

protocol ViewModelDelegate: AnyObject, AlertDelegate {
    func refreshView()
}

extension ViewModelDelegate {
    func refreshView() { }
}

