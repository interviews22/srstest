//
//  SignatureView.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//

import UIKit

/// Model object to hold signature
struct Line {
    let strokeWidth: Float = 1
    let color: UIColor = UIColor.black
    var points: [CGPoint] = []
}

/// Signature view
class SignatureView: UIView {
    private lazy var clearButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "trash"), for: .normal)
        button.addTarget(self, action: #selector(clear), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    private var placeHolderLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.text = Constants.signHere.text
        label.font = UIFont(name: "Bradley Hand Bold", size: 34)
        label.textAlignment = .center
        return label
    }()
    private var lines = [Line]() {
        didSet {
            clearButton.isHidden = lines.isEmpty
            placeHolderLabel.isHidden = !lines.isEmpty
        }
    }
    
    // MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }
   
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        lines.forEach { line in
            context.setStrokeColor(line.color.cgColor)
            context.setLineWidth(CGFloat(line.strokeWidth))
            context.setLineCap(.round)
            for (index, point) in line.points.enumerated() {
                if index == 0 {
                    context.move(to: point)
                } else {
                    context.addLine(to: point)
                }
            }
            context.strokePath()
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        lines.append(Line())
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self),
              var lastLine = lines.popLast() else {
                  return
              }
        lastLine.points.append(point)
        lines.append(lastLine)
        setNeedsDisplay()
    }
    
    /// Method to style view
    private func configureView() {
        addSubview(clearButton)
        addConstraintsWithFormat("V:[v0(32)]-(0)-|", views: clearButton)
        addConstraintsWithFormat("H:[v0(32)]-(0)-|", views: clearButton)
        
        addSubview(placeHolderLabel)
        addConstraintsWithFormat("V:|-(8)-[v0]-(0)-|", views: placeHolderLabel)
        addConstraintsWithFormat("H:|-(8)-[v0]-(0)-|", views: placeHolderLabel)
    }
    
    /// Method to reset signature
    @objc func clear() {
        lines.removeAll()
        setNeedsDisplay()
    }
}




extension SignatureView {
    /// Method to retrieve captured signature
    /// - Returns: Signature
    func getSignature() -> Signature {
        // Invalid signature
        func failure() -> Signature {
            return Signature(isValid: false,
                             image: UIImage())
        }
        
        // Prepare signature capturing
        guard !lines.isEmpty else {
            return failure()
        }
        clearButton.isHidden = true
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        clearButton.isHidden = false
        guard let image = image else {
            return failure()
        }
        return Signature(isValid: true,
                         image: image)
    }
}
