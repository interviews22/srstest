//
//  Utility.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import UIKit

struct Utility {
    static let shared = Utility()
    private init() { }
    
    /// Method to set minimum date
    /// - Returns: Date?
    func getDateOfBirthMinDate() -> Date? {
        let calendar = Calendar(identifier: .gregorian)
        var components = DateComponents()
        components.calendar = calendar
        components.year = -90
        guard let minDate = calendar.date(byAdding: components, to: Date()) else {
            return nil
        }
        return minDate
    }
    
    /// Method to set minimum date
    /// - Returns: Date?
    func getDateOfBirthMaxDate() -> Date? {
        let calendar = Calendar(identifier: .gregorian)
        var components = DateComponents()
        components.calendar = calendar
        components.year = -18
        guard let maxDate = calendar.date(byAdding: components, to: Date()) else {
            return nil
        }
        return maxDate
    }
}
