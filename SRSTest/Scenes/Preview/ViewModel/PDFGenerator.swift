//
//  PDFCreator.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import PDFKit
import UIKit

class PDFGenerator: NSObject {
    let user: User
    let title = Constants.termsAndConditions.text
    let body = LongTexts.consent
    let image = UIImage(named: "logo")!
    
    /// Custom init
    /// - Parameters
    ///     - user: User
    init(user: User) {
        self.user = user
    }
    
    /// Method to create pdf data
    /// - Returns: Data
    func generate() -> Data {
        // MetaData
        let metaData = [kCGPDFContextCreator: Constants.creator.text,
                         kCGPDFContextAuthor: Constants.author.text,
                          kCGPDFContextTitle: title]
        let format = UIGraphicsPDFRendererFormat()
        format.documentInfo = metaData as [String: Any]
        
        // Set Size
        let pageWidth = 8.5 * 72.0
        let pageHeight = 11 * 72.0
        let pageRect = CGRect(x: 0,
                              y: 0,
                              width: pageWidth,
                              height: pageHeight)
        
        // Prepare Page Generation
        let renderer = UIGraphicsPDFRenderer(bounds: pageRect,
                                             format: format)
        let data = renderer.pdfData { (context) in
            context.beginPage()
            let image = addImage(pageRect: pageRect,
                                 imageTop: 24)
            addBodyText(pageRect: pageRect,
                        textTop: image + 24)
            
            let fullName = "\(user.firstName ?? "") \(user.lastName ?? "")"
            let name = addUserText(pageRect: pageRect,
                                   textTop: pageRect.height - 300,
                                   text: fullName)
            let dob = addUserText(pageRect: pageRect,
                                  textTop: name + 4,
                                  text: "DOB: \(user.dob ?? "")")
            let phoneNumber = addUserText(pageRect: pageRect,
                                          textTop: dob + 4,
                                          text: "Cell: \(user.phoneNumber ?? "")")
            addUserSignature(pageRect: pageRect,
                             imageTop: phoneNumber + 8)
        }
        return data
    }
    
    /// Method to add image
    /// - Parameters:
    ///     - pageRect: CGRect
    ///     - imageTop: CGFloat
    /// - Returns: CGFloat
    func addImage(pageRect: CGRect,
                  imageTop: CGFloat) -> CGFloat {
        let width: CGFloat = 240
        let height: CGFloat = 160
        let imageRect = CGRect(x: (pageRect.width - width) / 2.0,
                               y: imageTop,
                               width: width,
                               height: height)
        image.draw(in: imageRect)
        return imageRect.origin.y + imageRect.size.height
    }
    
    /// Method to add body text
    /// - Parameters:
    ///     - pageRect: CGRect
    ///     - textTop: CGFloat
    func addBodyText(pageRect: CGRect,
                     textTop: CGFloat) {
        let textFont = UIFont.systemFont(ofSize: 14.0)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .justified
        paragraphStyle.lineBreakMode = .byWordWrapping
        let textAttributes = [NSAttributedString.Key.paragraphStyle: paragraphStyle,
                              NSAttributedString.Key.font: textFont]
        let attributedText = NSAttributedString(string: body, attributes: textAttributes)
        let textRect = CGRect(x: 16,
                              y: textTop,
                              width: pageRect.width - 32,
                              height: pageRect.height - textTop - pageRect.height / 5.0)
        attributedText.draw(in: textRect)
    }
    
    /// Method to add user info
    /// - Parameters:
    ///     - pageRect: CGRect
    ///     - textTop: CGFloat
    ///     - text: String
    /// - Returns: CGFloat
    func addUserText(pageRect: CGRect,
                     textTop: CGFloat,
                     text: String) -> CGFloat {
        let font = UIFont.systemFont(ofSize: 17.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedText = NSAttributedString(string: text, attributes: attributes)
        let textSize = attributedText.size()
        let textRect = CGRect(x: 16,
                              y: textTop,
                              width: textSize.width,
                              height: textSize.height)
        attributedText.draw(in: textRect)
        return textRect.origin.y + textRect.size.height
    }
    
    /// Method to add user signature
    /// - Parameters:
    ///     - pageRect: CGRect
    ///     - imageTop: CGFloat
    func addUserSignature(pageRect: CGRect,
                          imageTop: CGFloat) {
        let width: CGFloat = 120
        let height: CGFloat = 100
        let imageRect = CGRect(x: 16,
                               y: imageTop,
                               width: width,
                               height: height)
        guard let imageData = user.signature,
              let image = UIImage(data: imageData) else {
                  return 
              }
        image.draw(in: imageRect)
    }
}
