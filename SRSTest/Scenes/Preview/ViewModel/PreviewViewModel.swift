//
//  PreviewViewModel.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import Foundation

struct PreviewViewModel {
    let user: User
    
    /// Initialiser
    ///  - Parameters:
    ///      - user: User
    init(user: User) {
        self.user = user
    }
    
    /// Method to generate pdf data
    /// - Returns: Data
    func generatePDF() -> Data {
        let engine = PDFGenerator(user: self.user)
        return engine.generate()
    }
}
