//
//  PreviewViewController.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import PDFKit
import UIKit


class PreviewViewController: BaseViewController, StoryboardInitializable {
    @IBOutlet weak var pdfView: PDFView!
    
    var viewModel: PreviewViewModel!
    
    /// Method to style view
    override func configureView() {
     
        title = Constants.preview.text
    }
        
    /// Method to load data
    override func loadData() {
        pdfView.document = PDFDocument(data: viewModel.generatePDF())
        pdfView.autoScales = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
}
