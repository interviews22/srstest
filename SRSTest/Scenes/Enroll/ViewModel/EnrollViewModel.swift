//
//  EnrollViewModel.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//

class EnrollViewModel {
    
    weak var delegate: EnrollViewModelDelegate?
    
    /// Custom init
    init(delegate: EnrollViewModelDelegate?) {
        self.delegate = delegate
    }
    
    /// Method to prepare submission
    func submit(firstName: String?,
                lastName: String?,
                phoneNumber: String?,
                dateOfBirth: String?,
                signature: Signature?) {
        
        // Validation
        guard let firstName = firstName,
              firstName.isValidFirstName else {
                  delegate?.failure(message: Constants.invalidFirstName.text)
                  return
              }
        guard let lastName = lastName,
              lastName.isValidLastName else {
                  delegate?.failure(message: Constants.invalidLastName.text)
                  return
              }
        guard let phoneNumber = phoneNumber,
              phoneNumber.isValidPhoneNumber else {
                  delegate?.failure(message: Constants.invalidPhoneNumber.text)
                  return
              }
        guard let dateOfBirth = dateOfBirth,
              !dateOfBirth.isEmpty else {
                  delegate?.failure(message: Constants.invalidDateOfBirth.text)
                  return
              }
        guard let signature = signature,
              signature.isValid,
              let signatureData = signature.image.jpegData(compressionQuality: 1) else {
                  delegate?.failure(message: Constants.invalidSignature.text)
                  return
              }
        
        // Prepare to save
        DBManager.shared.saveUser(firstName: firstName,
                                  lastName: lastName,
                                  phoneNumber: phoneNumber,
                                  dateOfBirth: dateOfBirth,
                                  signature: signatureData) { [weak self] user in
            guard let user = user else {
                self?.delegate?.failure(message: Constants.userSaveError.text)
                return
            }
            self?.delegate?.refreshView()
            self?.delegate?.router.presentPreviewScene(user: user)
        }
    }
}
