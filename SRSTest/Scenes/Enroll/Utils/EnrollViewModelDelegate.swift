//
//  EnrollViewModelDelegate.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//

protocol EnrollViewModelDelegate: ViewModelDelegate {
    var router: EnrollSceneRouter { get set }
}
