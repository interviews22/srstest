//
//  Signature.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import UIKit

struct Signature {
    var isValid: Bool
    var image: UIImage
}
