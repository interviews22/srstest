//
//  EnrollViewController+Action.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import UIKit

extension EnrollViewController {
    
    @IBAction func submitAction(_ sender: UIButton) {
        view.endEditing(true)
        viewModel.submit(firstName: firstNameTextField.text,
                         lastName: lastNameTextField.text,
                         phoneNumber: phoneNumberTextField.text,
                         dateOfBirth: dateOfBirthTextField.text,
                         signature: signatureView.getSignature())
    }
    
    @IBAction func getListAction(_ sender: UIButton) {
        router.presetListScene()
    }
    
    @objc func dateDidSelect() {
        guard let datePicker = self.dateOfBirthTextField.inputView as? UIDatePicker else {
            return
        }
        dateOfBirthTextField.text = dateFormatter.string(from: datePicker.date)
        dateOfBirthTextField.resignFirstResponder()
    }
}
