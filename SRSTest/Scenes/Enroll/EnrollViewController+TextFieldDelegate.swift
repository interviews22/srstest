//
//  EnrollViewController+TextFieldDelegate.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//

import UIKit

extension EnrollViewController: UITextFieldDelegate {
    /// Method to observe textfield
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /// Performing phone number validation - Max digit length
        guard let text = textField.text else { return false }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        guard newString.count < 11 else {
            return false
        }
        textField.text = newString
        return false
    }
}
