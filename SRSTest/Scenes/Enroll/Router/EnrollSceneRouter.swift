//
//  EnrollRouter.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import UIKit

struct EnrollSceneRouter: Router {
    var presenter: UIViewController
    
    /// Custom Init
    init(presenter: UIViewController) {
        self.presenter = presenter
    }
    
    /// Method to present lists scene
    func presetListScene() {
        push(controller: UserListViewController.storyboardInstance)
    }
    
    /// Method to present PDF preview
    func presentPreviewScene(user: User) {
        let viewModel = PreviewViewModel(user: user)
        let controller = PreviewViewController.storyboardInstance
        controller.viewModel = viewModel
        push(controller: controller)
    }
}
