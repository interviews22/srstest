//
//  EnrollViewController.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//

import UIKit
import SkyFloatingLabelTextField

class EnrollViewController: BaseViewController, EnrollViewModelDelegate {
    // MARK: Connection Objects
    @IBOutlet weak var firstNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var dateOfBirthTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    
    var viewModel: EnrollViewModel!
    var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.preferredDatePickerStyle = .wheels
        picker.maximumDate = Utility.shared.getDateOfBirthMaxDate()
        picker.minimumDate = Utility.shared.getDateOfBirthMinDate()
        return picker
    }()
    var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = Constants.dateFormat.text
        return formatter
    }()
    lazy var router = EnrollSceneRouter(presenter: self)
    
    /// Method to style view
    override func configureView() {
        viewModel = EnrollViewModel(delegate: self)
        dateOfBirthTextField.inputView = datePicker
        dateOfBirthTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(dateDidSelect))
        submitButton.addShadow(offset: CGSize(width: 2, height: 2))
        listButton.addShadow(offset: CGSize(width: 0, height: 2))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    func refreshView() {
        clear()
    }
    
    func clear() {
        firstNameTextField.text = nil
        lastNameTextField.text = nil
        phoneNumberTextField.text = nil
        dateOfBirthTextField.text = nil
        signatureView.clear()
    }
}
