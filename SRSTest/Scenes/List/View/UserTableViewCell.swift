//
//  UserTableViewCell.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import UIKit

class UserTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    var data: User? {
        didSet {
            guard let data = data else {
                return
            }
            self.nameLabel.text = "\(data.firstName ?? "") \(data.lastName ?? "")"
            self.phoneNumberLabel.text = data.phoneNumber
            self.dobLabel.text = data.dob
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureView()
        clear()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        clear()
    }
    
    /// Method to style view
    func configureView() {
        selectionStyle = .none
        containerView.addShadow(color: .black,
                                opacity: 0.3,
                                offset: CGSize(width: 2, height: 2),
                                radius: 0)
        containerView.layer.borderWidth = 0.5
        containerView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    /// Method to reset data
    func clear() {
        nameLabel.text = nil
        phoneNumberLabel.text = nil
        dobLabel.text = nil
    }
}
