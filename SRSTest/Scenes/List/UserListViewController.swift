//
//  UserListViewController.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
import UIKit

class UserListViewController: BaseViewController, StoryboardInitializable, ViewModelDelegate, EmptyViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: UserListViewModel!
    lazy var router = UserListSceneRouter(presenter: self)
    var emptyView: EmptyView = {
        return EmptyView(image: #imageLiteral(resourceName: "empty"),
                         title: Constants.usersListIsEmpty.text)
    }()
    
    /// Method to style view
    override func configureView() {
        viewModel = UserListViewModel(self)
        title = Constants.users.text
        tableView.contentInset = UIEdgeInsets(top: 24,
                                              left: 0,
                                              bottom: 0,
                                              right: 0)
        addEmptyViewAsSubView(.lightGray)
    }
    
    /// Method which loads data for view
    override func loadData() {
        viewModel.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    /// Method which refresh the view to display latest data
    func refreshView() {
        tableView.reloadData()
        guard viewModel.numberOfRows == 0 else {
            hideEmptyView()
            return
        }
        showEmptyView()
    }
}




// MARK: Table Source And Delegates
extension UserListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: UserTableViewCell.identifier) as! UserTableViewCell
        cell.data = viewModel.dataAt(index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = viewModel.dataAt(index: indexPath.row)
        router.presentPreviewScene(user: user)
    }
}
