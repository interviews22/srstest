//
//  UserListViewModel.swift
//  SRSTest
//
//  Created by SRN on 4/3/22.
//
class UserListViewModel {
    
    weak var delegate: ViewModelDelegate?
    private var dataSource = [User]()
    var numberOfRows: Int {
        dataSource.count
    }
    
    /// Initialiser
    init(_ delegate: ViewModelDelegate?) {
        self.delegate = delegate
    }
    
    /// Method which loads data from data base
    func loadData() {
        DBManager.shared.fetchUsers { [weak self] users in
            self?.dataSource = users
            self?.delegate?.refreshView()
        }
    }
    
    /// Method which retrieve data from data source based on index.
    func dataAt(index: Int) -> User {
        return dataSource[index]
    }
}
